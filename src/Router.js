import React, { Suspense, lazy } from "react";
import { Router, Switch, Route } from "react-router-dom";
import { history } from "./history";
import { connect } from "react-redux";
import Spinner from "./components/@vuexy/spinner/Loading-spinner";
import { ContextLayout } from "./utility/context/Layout";

// Route-based code splitting
const Dashboard = lazy(() => import("./views/dashboard/Dashboard"));

const login = lazy(() => import("./views/pages/authentication/login/Login"));
const register = lazy(() =>
  import("./views/pages/authentication/register/Register")
);
const forgotPassword = lazy(() =>
  import("./views/pages/authentication/ForgotPassword")
);
const resetPassword = lazy(() =>
  import("./views/pages/authentication/ResetPassword")
);
const driver_list = lazy(() => import("./views/pages/drivers/List"));
const vehicle_list = lazy(() => import("./views/pages/vehicles/List"));
const offer_list = lazy(() => import("./views/pages/offers/List"));
const rating_list = lazy(() => import("./views/pages/rating/List"));
const driver_add = lazy(() => import("./views/pages/drivers/Add-driver"));


// Set Layout and Component Using App Route
const RouteConfig = ({
  component: Component,
  fullLayout,
  permission,
  user,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => {
      return (
        <ContextLayout.Consumer>
          {context => {
            let LayoutTag =
              fullLayout === true
                ? context.fullLayout
                : context.state.activeLayout === "horizontal"
                ? context.horizontalLayout
                : context.VerticalLayout;
            return (
              <LayoutTag {...props} permission={props.user}>
                <Suspense fallback={<Spinner />}>
                  <Component {...props} />
                </Suspense>
              </LayoutTag>
            );
          }}
        </ContextLayout.Consumer>
      );
    }}
  />
);
const mapStateToProps = state => {
  return {
    user: state.auth.login.userRole
  };
};

const AppRoute = connect(mapStateToProps)(RouteConfig);

class AppRouter extends React.Component {
  render() {
    return (
      // Set the directory path if you are deploying in sub-folder
      <Router basename={'/reactProject/pd/'} history={history}>
        
        <Switch>
          <AppRoute path="/login" component={login} fullLayout />
          <AppRoute path="/register" component={register} fullLayout />
          <AppRoute
            path="/forgot-password"
            component={forgotPassword}
            fullLayout
          />
          <AppRoute
            path="/reset-password"
            component={resetPassword}
            fullLayout
          />
          <AppRoute exact path="/" component={Dashboard} />
          <AppRoute path="/vehicles" component={vehicle_list} />
          <AppRoute path="/drivers" component={driver_list} />
          <AppRoute path="/driver-add" component={driver_add} />
          <AppRoute path="/offers" component={offer_list} />
          <AppRoute path="/ratings" component={rating_list} />
        </Switch>
      </Router>
    );
  }
}

export default AppRouter;
