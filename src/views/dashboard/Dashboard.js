import React from "react"
import { connect } from "react-redux"
import { Row, Col } from "reactstrap"
import SubscribersGained from "../ui-elements/cards/statistics/SubscriberGained"
import RevenueGenerated from "../ui-elements/cards/statistics/RevenueGenerated"
import QuaterlySales from "../ui-elements/cards/statistics/QuaterlySales"
import OrdersReceived from "../ui-elements/cards/statistics/OrdersReceived"




class Dashboard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    
      width : window.innerWidth
    }
  }

  updateWidth = () => {
    this.setState(prevState => ({
      width: window.innerWidth
    }))
  }

 componentDidMount() {
  if (window !== "undefined") {
    window.addEventListener("resize", this.updateWidth, false)
  }
 }
 

  render() {
    
    return (
      <React.Fragment>
        <Row className="match-height">
          <Col lg="3" md="6" sm="6">
            <SubscribersGained />
          </Col>
          <Col lg="3" md="6" sm="6">
            <RevenueGenerated />
          </Col>
          <Col lg="3" md="6" sm="6">
            <QuaterlySales />
          </Col>
          <Col lg="3" md="6" sm="6">
            <OrdersReceived />
          </Col>
        </Row>
      </React.Fragment>
    )
  }
}

// const mapStateToProps = state => {
//   return {
//     layout : state.customizer.customizer.layout
//   }
// }

// export default connect(mapStateToProps)(AnalyticsDashboard)
export default Dashboard;