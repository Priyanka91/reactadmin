import React from 'react';
import {
  Button,
  Card,
  CardBody,
  Row,
  Col,
  FormGroup,
  Input,
  Label
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import { Mail, Lock, Check, Facebook, Twitter, GitHub } from 'react-feather';
import { history } from '../../../../history';
import Checkbox from '../../../../components/@vuexy/checkbox/CheckboxesVuexy';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import {
  loginUser,
  logoutUser
} from '../../../../redux/actions/auth/loginActions';

import md5 from 'md5';
import sha256 from 'sha256';

import loginImg from '../../../../assets/img/pages/login.png';
import '../../../../assets/scss/pages/authentication.scss';

const LoginSchema = Yup.object().shape({
  email: Yup.string()
    .email('Invalid email address')
    .required('Email Required'),
  password: Yup.string().required('Password Required')
});

class Login extends React.Component {
  state = {
    activeTab: '1',
    email: '',
    password: ''
  };
  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  };
  render() {
    return (
      <Row className="m-0 justify-content-center">
        <Col
          sm="8"
          xl="7"
          lg="10"
          md="8"
          className="d-flex justify-content-center"
        >
          <Card className="bg-authentication login-card rounded-0 mb-0 w-100">
            <Row className="m-0">
              <Col
                lg="6"
                className="d-lg-block d-none text-center align-self-center px-1 py-0"
              >
                <img src={loginImg} alt="loginImg" />
              </Col>
              <Col lg="6" md="12" className="p-0">
                <Card className="rounded-0 mb-0 px-2">
                  <CardBody>
                    <h4>Login</h4>
                    <p>Welcome back, please login to your account.</p>

                    <Formik
                      initialValues={{
                        email: '',
                        password: ''
                      }}
                      validationSchema={LoginSchema}
                      onSubmit={(values, { setSubmitting, resetForm }) => {
                        // When button submits form and form is in the process of submitting, submit button is disabled
                        setSubmitting(true);
                        //history.push('/')
                        // values.password=md5(values.password);
                        // values.password=sha256(values.password);
                       // console.log(values)

                        this.props.loginUser(values);
                        // setTimeout(() => {
                        //   alert(JSON.stringify(values, null, 2));
                        //   resetForm();
                        //   setSubmitting(false);
                        // }, 500);
                      }}
                      render={({ errors, touched, values, setFieldValue }) => (
                        <Form>
                          <FormGroup className="form-label-group position-relative has-icon-left m-0 mt-2">
                            <Field
                              type="email"
                              name="email"
                              placeholder="Email"
                              className={`form-control ${
                                errors.email && touched.email
                                  ? 'is-invalid'
                                  : ''
                              }`}
                            />

                            <div className="form-control-position">
                              <Mail
                                size={15}
                                color={
                                  errors.email && touched.email
                                    ? '#ea5455'
                                    : 'black'
                                }
                              />
                            </div>
                            <Label>Email</Label>
                          </FormGroup>
                          <ErrorMessage
                            name="email"
                            component="label"
                            className="text-danger text-sm"
                          />
                          <FormGroup className="form-label-group position-relative has-icon-left m-0 mt-2">
                            <Field
                              type="password"
                              name="password"
                              placeholder="Password"
                              className={`form-control ${
                                errors.password && touched.password
                                  ? 'is-invalid'
                                  : ''
                              }`}
                            />
                            <div className="form-control-position">
                              <Lock
                                size={15}
                                color={
                                  errors.password && touched.password
                                    ? '#ea5455'
                                    : 'black'
                                }
                              />
                            </div>
                            <Label>Password</Label>
                          </FormGroup>
                          <ErrorMessage
                            name="password"
                            component="label"
                            className="text-danger text-sm"
                          />
                          <FormGroup className="d-flex justify-content-between  align-items-center m-0 mt-2 mb-1">
                            <Checkbox
                              color="primary"
                              name="RememberMe"
                              checked={values.RememberMe}
                              onChange={() =>
                                setFieldValue('RememberMe', !values.RememberMe)
                              }
                              icon={<Check className="vx-icon" size={16} />}
                              label="Remember me"
                            />

                            <div className="float-right">
                              <Link to="/forgot-password">
                                Forgot Password?
                              </Link>
                            </div>
                          </FormGroup>
                          <div className="d-flex justify-content-between">
                            <Button.Ripple color="primary" type="submit">
                              Login
                            </Button.Ripple>
                          </div>
                        </Form>
                      )}
                    />
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    );
  }
}
const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { loginUser })(Login);
