import React, { Component, StrictMode } from 'react';
import {
  Label,
  Input,
  FormGroup,
  Button,
  Row,
  Col,
  Table,
  CustomInput
} from 'reactstrap';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import { X } from 'react-feather';
import PerfectScrollbar from 'react-perfect-scrollbar';
import classnames from 'classnames';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import message from '../../../language/message';
import * as Yup from 'yup';

const SignUpSchema = Yup.object().shape({
  model: Yup.string().required('Model Required.'),
  productionYear: Yup.string().required('Production year Required.'),
  color: Yup.string().required('Color Required.'),
  licencePlate: Yup.string().required('Licence plate Required.'),
  serviceClass: Yup.string().required('Service class Required.')
});

class DataListSidebar extends Component {
  state = {
    vehicle_licence_img: '',
    vehicle_insurance_certificate_img: ''
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.data !== null && prevProps.data === null) {
      if (this.props.data.id !== prevState.id) {
        this.setState({ id: this.props.data.id });
      }
      if (this.props.data.name !== prevState.name) {
        this.setState({ name: this.props.data.name });
      }
      if (this.props.data.category !== prevState.category) {
        this.setState({ category: this.props.data.category });
      }
      if (this.props.data.popularity.popValue !== prevState.popularity) {
        this.setState({
          popularity: {
            ...this.state.popularity,
            popValue: this.props.data.popularity.popValue
          }
        });
      }
      if (this.props.data.order_status !== prevState.order_status) {
        this.setState({ order_status: this.props.data.order_status });
      }
      if (this.props.data.price !== prevState.price) {
        this.setState({ price: this.props.data.price });
      }
      if (this.props.data.img !== prevState.img) {
        this.setState({ img: this.props.data.img });
      }
    }
    if (this.props.data === null && prevProps.data !== null) {
      this.setState({
        id: '',
        name: '',
        category: 'Audio',
        order_status: 'pending',

        price: '',
        img: '',
        popularity: {
          popValue: ''
        }
      });
    }
    if (this.addNew) {
      this.setState({
        id: '',
        name: '',
        category: 'Audio',
        order_status: 'pending',
        price: '',
        img: '',
        popularity: {
          popValue: ''
        }
      });
    }
    this.addNew = false;
  }

  _handleImageChange = (e, type) => {
    // e.preventDefault();

    // FileList to Array
    let files = Array.from(e.target.files);

    // File Reader for Each file and and update state arrays
    files.forEach((file, i) => {
      let reader = new FileReader();

      reader.onloadend = () => {
        if (type == 'licence') {
          this.setState({
            vehicle_licence_img: reader.result
          });
        } else if (type == 'certificate') {
          this.setState({
            vehicle_insurance_certificate_img: reader.result
          });
        }
      };

      reader.readAsDataURL(file);
    });
  };

  handleSubmit = obj => {
    toast.success('This is success toast!');
    if (this.props.data !== null) {
      this.props.updateData(obj);
    } else {
      this.addNew = true;
      this.props.addData(obj);
    }
    let params = Object.keys(this.props.dataParams).length
      ? this.props.dataParams
      : { page: 1, perPage: 4 };
    this.props.handleSidebar(false, true);
    this.props.getData(params);
  };

  resetErrors = setErrors => {
    setErrors({});
  };

  render() {
    let { show, handleSidebar, data } = this.props;
    let {
      name,
      category,
      order_status,
      price,
      popularity,
      vehicle_licence_img,
      vehicle_insurance_certificate_img,
      img,
      imagePreviewUrl,
      check_driver
    } = this.state;
    return (
      <React.Fragment>
        <Formik
          initialValues={{
            model: '',
            productionYear: '',
            color: '',
            licencePlate: '',
            serviceClass: ''
          }}
          validationSchema={SignUpSchema}
          onSubmit={(values, { setSubmitting, resetForm }) => {
            // When button submits form and form is in the process of submitting, submit button is disabled
            setSubmitting(true);

            // Simulate submitting to database, shows us values submitted, resets form
            setTimeout(() => {
              alert(JSON.stringify(values, null, 2));
              resetForm();
              setSubmitting(false);
            }, 500);
          }}
          render={({
            errors,
            touched,
            values,
            setFieldValue,
            isSubmitting,
            handleChange,
            handleBlur,
            setErrors
          }) => (
            <Form>
              <div
                className={classnames('data-list-sidebar', {
                  show: show
                })}
              >
                <div className="data-list-sidebar-header mt-2 px-2 d-flex justify-content-between">
                  <h4>
                    {data !== null ? 'UPDATE VEHICLE' : 'ADD NEW VEHICLE'}
                  </h4>
                  <X size={20} onClick={() => {
                    this.resetErrors(setErrors)
                    handleSidebar(false, true)
                    }} />
                </div>
                <PerfectScrollbar
                  className="data-list-fields px-2 mt-3"
                  options={{ wheelPropagation: false }}
                >
                  <FormGroup>
                    <Label for="data-type">Model</Label>
                    <Input
                      type="select"
                      name="model"
                      placeholder="Black"
                      value={values.model}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={`form-control ${
                        errors.model && touched.model ? 'is-invalid' : ''
                      }`}
                    >
                      <option value="" label="Select a Model" />
                      <option value="Tesla Model X">Tesla Model X</option>
                      <option value="BMW Model X5">BMW Model X5</option>
                    </Input>
                    <ErrorMessage
                      name="model"
                      component="label"
                      className="text-danger text-sm"
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label for="data-type">Production year</Label>
                    <Input
                      type="select"
                      name="productionYear"
                      placeholder="Black"
                      value={values.productionYear}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={`form-control ${
                        errors.productionYear && touched.productionYear
                          ? 'is-invalid'
                          : ''
                      }`}
                    >
                      <option value="" label="Select a Production year" />
                      <option value="2012">2012</option>
                      <option value="2014">2014</option>
                    </Input>
                    <ErrorMessage
                      name="productionYear"
                      component="label"
                      className="text-danger text-sm"
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label for="data-type">Color</Label>
                    <Input
                      type="select"
                      name="color"
                      placeholder="Black"
                      value={values.color}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={`form-control ${
                        errors.color && touched.color ? 'is-invalid' : ''
                      }`}
                    >
                      <option value="" label="Select a color" />
                      <option value="Black">Black</option>
                      <option value="White">White</option>
                    </Input>
                    <ErrorMessage
                      name="color"
                      component="label"
                      className="text-danger text-sm"
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label for="data-type">Licence Plate</Label>
                    <Field
                      type="text"
                      name="licencePlate"
                      placeholder="RJ19CA5365"
                      className={`form-control ${
                        errors.licencePlate && touched.licencePlate
                          ? 'is-invalid'
                          : ''
                      }`}
                    />
                    <ErrorMessage
                      name="licencePlate"
                      component="label"
                      className="text-danger text-sm"
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label for="data-type">Service Class</Label>
                    <Field
                      type="text"
                      name="serviceClass"
                      disabled
                      placeholder="business class"
                      className={`form-control ${
                        errors.serviceClass && touched.serviceClass
                          ? 'is-invalid'
                          : ''
                      }`}
                    />
                    <ErrorMessage
                      name="serviceClass"
                      component="label"
                      className="text-danger text-sm"
                    />
                  </FormGroup>
                  <Label for="data-email">Vehicle license</Label>
                  <FormGroup>
                    <img
                      className="img-fluid w-50 mt-1"
                      src={vehicle_licence_img}
                      alt={name}
                    />

                    <div className="d-flex flex-wrap justify-content-between mt-1">
                      <label
                        className="btn btn-primary"
                        htmlFor="vehicle-licence"
                        color="primary"
                      >
                        Upload Image
                        <input
                          type="file"
                          id="vehicle-licence"
                          hidden
                          onChange={e => this._handleImageChange(e, 'licence')}
                        />
                      </label>

                      {vehicle_licence_img.length <= 0 ? null : (
                        <Button
                          color="danger"
                          onClick={() =>
                            this.setState({ vehicle_licence_img: '' })
                          }
                        >
                          Remove Image
                        </Button>
                      )}
                    </div>
                  </FormGroup>
                  <Label for="data-email">Vehicle Insurance Certificate</Label>
                  <FormGroup>
                    <img
                      className="img-fluid w-50 mt-1"
                      src={vehicle_insurance_certificate_img}
                      alt={name}
                    />

                    <div className="d-flex flex-wrap justify-content-between mt-1">
                      <label
                        className="btn btn-primary"
                        htmlFor="vehicle-insurance-certificate"
                        color="primary"
                      >
                        Upload Image
                        <input
                          type="file"
                          id="vehicle-insurance-certificate"
                          hidden
                          onChange={e =>
                            this._handleImageChange(e, 'certificate')
                          }
                        />
                      </label>
                      {vehicle_insurance_certificate_img.length <= 0 ? null : (
                        <Button
                          color="danger"
                          onClick={() =>
                            this.setState({
                              vehicle_insurance_certificate_img: ''
                            })
                          }
                        >
                          Remove Image
                        </Button>
                      )}
                    </div>
                  </FormGroup>
                </PerfectScrollbar>
                <div className="data-list-sidebar-footer px-2 d-flex justify-content-start align-items-center mt-1">
                  <Button
                    color="primary"
                    // onClick={() => this.handleSubmit(this.state)}
                    type="submit"
                    disabled={isSubmitting}
                  >
                    {data !== null ? 'Update' : 'Submit'}
                  </Button>
                  <Button
                    className="ml-1"
                    color="danger"
                    outline
                    onClick={() => {
                      this.resetErrors(setErrors)
                      handleSidebar(false, true)
                    }}
                  >
                    Cancel
                  </Button>
                </div>
              </div>
            </Form>
          )}
        />
        <ToastContainer />
      </React.Fragment>
    );
  }
}
export default DataListSidebar;
