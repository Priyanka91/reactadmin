import React, { Component, StrictMode } from 'react';

import {
  Label,
  Input,
  FormGroup,
  Button,
  Card,
  CardBody,
  Row,
  Col,
  Table,
  CustomInput
} from 'reactstrap';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import { X } from 'react-feather';
import PerfectScrollbar from 'react-perfect-scrollbar';
import classnames from 'classnames';
import { ToastContainer, toast } from 'react-toastify';
import Checkbox from '../../../components/@vuexy/checkbox/CheckboxesVuexy';
import 'react-toastify/dist/ReactToastify.css';
import { Lock, Check } from 'react-feather';
import message from '../../../language/message';
import * as Yup from 'yup';
import Bowser from 'bowser';
import DropzoneComponent from 'react-dropzone-component';
import 'dropzone/dist/dropzone.css';
import 'react-dropzone-component/styles/filepicker.css';
import { DeviceUUID } from 'device-uuid';
import Dropzone from 'react-dropzone';
import ImageDrop from './image';

const SignUpSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(3, 'Must be longer than 3 characters')
    .max(12, 'Nice try, nobody has a first name that long')
    .required('First name Required'),
  lastName: Yup.string()
    .min(3, 'Must be longer than 3 characters')
    .max(12, 'Nice try, nobody has a last name that long')
    .required('Last name Required'),
  email: Yup.string()
    .email('Invalid email address')
    .required('Email Required'),
  phoneNumber: Yup.string().required('Phone Number Required'),
  password: Yup.string()
    .label('Password')
    .required()
    .min(2, 'Seems a bit short...')
    .max(10, 'We prefer insecure system, try a shorter password.'),
  confirmPassword: Yup.string()
    .required()
    .label('Confirm password')
    .test('passwords-match', 'Passwords must match ya fool', function(value) {
      return this.parent.password === value;
    }),
  checkProvider: Yup.bool()
    .required()
    .test('check-box', 'Please select one permission.', function(value) {
      return (
        this.parent.checkDispatcher ||
        this.parent.checkDriver ||
        this.parent.checkAccountant ||
        value
      );
    }),
  checkDispatcher: Yup.bool()
    .required()
    .test('check-box', 'Please select one permission.', function(value) {
      return (
        this.parent.checkProvider ||
        this.parent.checkDriver ||
        this.parent.checkAccountant ||
        value
      );
    }),
  checkDriver: Yup.bool()
    .required()

    .test('check-box', 'Please select one permission.', function(value) {
      return (
        this.parent.checkProvider ||
        this.parent.checkDispatcher ||
        this.parent.checkAccountant ||
        value
      );
    }),
  checkAccountant: Yup.bool()
    .required()
    .test('check-box', 'Please select one permission.', function(value) {
      return (
        this.parent.checkProvider ||
        this.parent.checkDispatcher ||
        this.parent.checkDriver ||
        value
      );
    })
});

class DataListSidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      check_admin: false,
      check_driver: false,
      check_manager: false,
      driver_licence_img: '',
      driver_practice_permit_img: '',
      profile_img: ''
    };
    this.onDrop = files => {
      this.setState({ files });
    };
    this.state = {
      files: []
    };

    this.djsConfig = {
      addRemoveLinks: true,
      acceptedFiles: 'image/jpeg,image/png,image/gif'
    };

    this.componentConfig = {
      iconFiletypes: ['.jpg', '.png', '.gif'],
      showFiletypeIcon: true,
      postUrl: '/uploadHandler'
    };

    // If you want to attach multiple callbacks, simply
    // create an array filled with all your callbacks.
    this.callbackArray = [() => console.log('Hi!'), () => console.log('Ho!')];

    // Simple callbacks work too, of course
    this.callback = () => console.log('Hello!');

    this.success = file => console.log('uploaded', file);

    this.removedfile = file => console.log('removing...', file);

    this.dropzone = null;
  }

  toggleTooltip = () => {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  };

  _handleImageChange = (e, type) => {
    // e.preventDefault();

    // FileList to Array
    let files = Array.from(e.target.files);

    // File Reader for Each file and and update state arrays
    files.forEach((file, i) => {
      let reader = new FileReader();

      reader.onloadend = () => {
        if (type == 'licence') {
          this.setState({
            driver_licence_img: reader.result
          });
        } else if (type == 'permit') {
          this.setState({
            driver_practice_permit_img: reader.result
          });
        } else if (type == 'profile') {
          this.setState({
            profile_img: reader.result
          });
        }
      };

      reader.readAsDataURL(file);
    });
  };

  addNew = false;

  componentDidUpdate(prevProps, prevState) {
    var uuid = new DeviceUUID().get();
    console.log('default uuid' + uuid);
    var du = new DeviceUUID().parse();
    var dua = [
      du.language,
      //n du.platform,
      du.os,
      du.cpuCores,
      du.isAuthoritative,
      du.silkAccelerated,
      du.isKindleFire,
      du.isDesktop,
      du.isMobile,
      du.isTablet,
      du.isWindows,
      du.isLinux,
      du.isLinux64,
      du.isMac,
      du.isiPad,
      du.isiPhone,
      du.isiPod,
      du.isSmartTV,
      du.pixelDepth,
      du.isTouchScreen
    ];
    var uuid = du.hashMD5(dua.join(':'));
    console.log('custom uuid' + uuid, 'jfsg', du.platform);

    console.log(Bowser.parse(window.navigator.userAgent));
    if (this.props.data !== null && prevProps.data === null) {
      if (this.props.data.id !== prevState.id) {
        this.setState({ id: this.props.data.id });
      }
      if (this.props.data.name !== prevState.name) {
        this.setState({ name: this.props.data.name });
      }
      if (this.props.data.category !== prevState.category) {
        this.setState({ category: this.props.data.category });
      }
      if (this.props.data.popularity.popValue !== prevState.popularity) {
        this.setState({
          popularity: {
            ...this.state.popularity,
            popValue: this.props.data.popularity.popValue
          }
        });
      }
      if (this.props.data.order_status !== prevState.order_status) {
        this.setState({ order_status: this.props.data.order_status });
      }
      if (this.props.data.price !== prevState.price) {
        this.setState({ price: this.props.data.price });
      }
      if (this.props.data.img !== prevState.img) {
        this.setState({ img: this.props.data.img });
      }
    }
    if (this.props.data === null && prevProps.data !== null) {
      this.setState({
        id: '',
        name: '',
        category: 'Audio',
        order_status: 'pending',
        price: '',
        img: '',
        popularity: {
          popValue: ''
        }
      });
    }
    if (this.addNew) {
      this.setState({
        id: '',
        name: '',
        category: 'Audio',
        order_status: 'pending',
        price: '',
        img: '',
        popularity: {
          popValue: ''
        }
      });
    }
    this.addNew = false;
  }

  handleSubmit = obj => {
    toast.success('This is success toast!');
    if (this.props.data !== null) {
      this.props.updateData(obj);
    } else {
      this.addNew = true;
      this.props.addData(obj);
    }
    let params = Object.keys(this.props.dataParams).length
      ? this.props.dataParams
      : { page: 1, perPage: 4 };
    this.props.handleSidebar(false, true);
    this.props.getData(params);
  };

  resetErrors = setErrors => {
    console.log(setErrors);
    setErrors({});
  };
  render() {
    let { show, handleSidebar, data } = this.props;
    let {
      name,
      category,
      order_status,
      price,
      popularity,
      driver_licence_img,
      driver_practice_permit_img,
      profile_img,
      img,
      imagePreviewUrl,
      check_driver
    } = this.state;
    return (
      <React.Fragment>
        <Formik
          initialValues={{
            firstName: '',
            lastName: '',
            email: '',
            phoneNumber: '',
            password: '',
            confirmPassword: '',
            checkDriver: false,
            checkProvider: false,
            checkDispatcher: false,
            checkAccountant: false
          }}
          validationSchema={SignUpSchema}
          onSubmit={(values, { setSubmitting, resetForm }) => {
            // When button submits form and form is in the process of submitting, submit button is disabled
            setSubmitting(true);

            // Simulate submitting to database, shows us values submitted, resets form
            setTimeout(() => {
              alert(JSON.stringify(values, null, 2));
              resetForm();
              setSubmitting(false);
            }, 500);
          }}
          render={({
            errors,
            touched,
            values,
            setFieldValue,
            isSubmitting,
            setErrors
          }) => (
            <Card>
              <CardBody>
                <Form>
                  <div className="">
                    <h4>
                      {/* data */ null !== null
                        ? 'UPDATE DRIVER'
                        : 'ADD NEW DRIVER'}
                    </h4>
                  </div>

                  <Row>
                    <FormGroup className="col-6">
                      <Label for="data-name">First name</Label>
                      <Field
                        type="text"
                        name="firstName"
                        placeholder="firstName"
                        className={`form-control ${
                          errors.firstName && touched.firstName
                            ? 'is-invalid'
                            : ''
                        }`}
                      />
                      <ErrorMessage
                        name="firstName"
                        component="label"
                        className="text-danger text-sm"
                      />
                    </FormGroup>

                    <FormGroup className="col-6">
                      <Label for="data-name">Last name</Label>
                      <Field
                        type="text"
                        name="lastName"
                        placeholder="Last Name"
                        className={`form-control ${
                          errors.lastName && touched.lastName
                            ? 'is-invalid'
                            : ''
                        }`}
                      />
                      <ErrorMessage
                        name="lastName"
                        component="label"
                        className="text-danger text-sm"
                      />
                    </FormGroup>
                  </Row>
                  <Row>
                    <FormGroup className="col-6">
                      <Label for="data-email">Email</Label>

                      <Field
                        type="email"
                        name="email"
                        placeholder="email"
                        className={`form-control ${
                          errors.email && touched.email ? 'is-invalid' : ''
                        }`}
                      />
                      <ErrorMessage
                        name="email"
                        component="label"
                        className="text-danger text-sm"
                      />
                    </FormGroup>
                    <FormGroup className="col-6">
                      <Label for="data-email">Phone number</Label>
                      <Field
                        type="number"
                        name="phoneNumber"
                        placeholder="phoneNumber"
                        className={`form-control ${
                          errors.phoneNumber && touched.phoneNumber
                            ? 'is-invalid'
                            : ''
                        }`}
                      />
                      <ErrorMessage
                        name="phoneNumber"
                        component="label"
                        className="text-danger text-sm"
                      />
                    </FormGroup>
                  </Row>
                  
                  <Row>
                    <FormGroup className="col-6">
                      <Label for="data-email">Password</Label>
                      <Field
                        type="password"
                        name="password"
                        placeholder="password"
                        className={`form-control ${
                          errors.password && touched.password
                            ? 'is-invalid'
                            : ''
                        }`}
                      />
                      <ErrorMessage
                        name="password"
                        component="label"
                        className="text-danger text-sm"
                      />
                    </FormGroup>
                    <FormGroup className="col-6">
                    <Label for="data-email">Profile image</Label>
                    <ImageDrop />
                  </FormGroup>
                   
                  </Row>
                  <FormGroup>
                    <Label className="d-block mb-50" for="communication">
                      Permission
                    </Label>
                    <p>
                      Please note that you can assign different levels of
                      authorization to your team. You can choose whether your
                      chauffeurs have visibility of the open offers or only the
                      assignments you already accepted.
                    </p>
                    <div className="d-flex">
                      <Checkbox
                        color="primary"
                        name="checkProvider"
                        icon={<Check className="vx-icon" size={16} />}
                        checked={values.checkProvider}
                        onChange={() =>
                          setFieldValue('checkProvider', !values.checkProvider)
                        }
                        label="Provider - Accept offers (prices visible)"
                        value="Provider"
                      />
                    </div>
                    <div className="d-flex">
                      <Checkbox
                        color="primary"
                        name="checkDispatcher"
                        icon={<Check className="vx-icon" size={16} />}
                        checked={values.checkDispatcher}
                        onChange={() =>
                          setFieldValue(
                            'checkDispatcher',
                            !values.checkDispatcher
                          )
                        }
                        label="Dispatcher - Assign and manage company rides"
                        value="Dispatcher"
                      />
                    </div>
                    <div className="d-flex">
                      <Checkbox
                        color="primary"
                        icon={<Check className="vx-icon" size={16} />}
                        label="Driver - Conduct rides"
                        name="checkDriver"
                        checked={values.checkDriver}
                        onChange={() =>
                          setFieldValue('checkDriver', !values.checkDriver)
                        }
                      />
                    </div>
                    <div className="d-flex">
                      <Checkbox
                        color="primary"
                        name="checkAccountant"
                        icon={<Check className="vx-icon" size={16} />}
                        label="Accountant - Review report"
                        checked={values.checkAccountant}
                        onChange={() =>
                          setFieldValue(
                            'checkAccountant',
                            !values.checkAccountant
                          )
                        }
                        value="Accountant"
                      />
                    </div>

                    <ErrorMessage
                      name="checkDriver"
                      component="label"
                      className="text-danger text-sm"
                    />
                  </FormGroup>
                  {values.checkDriver ? (
                    <React.Fragment>
                      <Row>
                        <FormGroup className="col-6">
                          <Label for="data-email">Driver license</Label>
                          <ImageDrop />
                        </FormGroup>
                        <FormGroup className="col-6">
                          <Label for="data-email">
                            Driving Practice Permit
                          </Label>
                          <ImageDrop />
                        </FormGroup>
                      </Row>
                    </React.Fragment>
                  ) : null}

                  <div className="data-list-sidebar-footer px-2 d-flex justify-content-start align-items-center mt-1">
                    <Button
                      color="primary"
                      // onClick={() => this.handleSubmit(this.state)}
                      type="submit"
                      disabled={isSubmitting}
                    >
                      {null !== null ? 'Update' : 'Submit'}
                    </Button>
                   
                  </div>
                </Form>
              </CardBody>
            </Card>
          )}
        />
        <ToastContainer />
      </React.Fragment>
    );
  }
}
export default DataListSidebar;
