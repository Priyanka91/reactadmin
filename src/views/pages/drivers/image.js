import React, { useEffect, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import {

    Row,

  } from 'reactstrap';

const thumbsContainer = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap'
};

const thumb = {
  display: 'inline-flex',
  borderRadius: 2,
  border: '1px solid #eaeaea',
  marginBottom: 8,
  marginRight: 8,
  width: "auto",
  height: "auto",
  padding: 4,
  boxSizing: 'border-box'
};

const thumbInner = {
  display: 'flex',
  minWidth: 0,
  overflow: 'hidden'
};


function Previews(props) {
  const [files, setFiles] = useState([]);
  const { getRootProps, getInputProps } = useDropzone({
    accept: 'image/*',
    onDrop: acceptedFiles => {
      setFiles(
        acceptedFiles.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file)
          })
        )
      );
    }
  });

  const thumbs = files.map(file => (
    <div style={thumb} key={file.name}>
      <div style={thumbInner}>
        <img className="img-fluid" src={file.preview}  />
      </div>
    </div>
  ));

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach(file => URL.revokeObjectURL(file.preview));
    },
    [files]
  );

  return (
    <React.Fragment>
        <Row>
        
      <div  className="col-7" >
<div  {...getRootProps({ className: 'dropzone text-center' })}>
        <input {...getInputProps()} />
        <img className="text center" width="100px" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcREJW_fYX9ctIrK1K8hMD4eaVRAWxSwEF_ccBu8EK7qxHPwWMIn"/> 
        <p>Drag 'n' drop some file here, or click to select files</p>
      </div>
      </div>
      <aside className="col-5" style={thumbsContainer}>{thumbs}</aside>
      </Row>
      
    </React.Fragment>
  );
}
export default Previews;
