const messageObj = {
  driver_list_heading: 'Still No any driver registered our system.',
  driver_list_subheading:
    'Please make your driver, after that you can assign the booking.',
  email_use_for_login: 'This email is use for login.'
};

module.exports = messageObj;
