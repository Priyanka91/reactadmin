// import axios from "axios"

// export const login = params => {
//   return async dispatch => {
//     await axios.get("/api/datalist/data", params).then(response => {
//       dispatch({
//         type: "GET_DATA",
//         data: response.data.data,
//         totalPages: response.data.totalPages,
//         params
//       })
//     })
//   }
// }
// export const changeRole = role => {
//   return dispatch => dispatch({ type: "CHANGE_ROLE", userRole: role })
// }



import axios from "axios";
import setAuthToken from "../../../utility/setAuthToken";
//import jwt_decode from "jwt-decode";

import { GET_ERRORS, SET_CURRENT_USER, USER_LOADING } from "../../reducers/auth/loginReducer";

// Register User
export const registerUser = (userData, history) => dispatch => {
  axios
    .post("", userData)
    .then(res => history.push("/login"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

const base_url = process.env.MIX_API_BASE_URL;
// Login - get user token
export const loginUser = userData => dispatch => {
  console.log(userData)
  const token = '';
  axios
    .post("http://3.20.90.174:5000/api/entrance/login", userData)
    .then(res => {
      console.log(res)
      // Save to localStorage

      // Set token to localStorage
      // const  token  = res.data.data.oauth_token;
      /*console.log('Message');
      console.log(res.data.message);*/

      //   dispatch({
      //       type: GET_ERRORS,
      //       payload: res.data.message
      //   });
      //  if(res.data.data.oauth_token)
      //  {
      //      localStorage.setItem("Oauth", token);
      //      // Set token to Auth header
      //     setAuthToken(token);
      //      // Decode token to get user data
      //      const decoded = token;
      //      // Set current user
      //      dispatch(setCurrentUser(decoded));
      //  }

    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Set logged in user
export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};

// User loading
export const setUserLoading = () => {
  return {
    type: USER_LOADING
  };
};

// Log user out
export const logoutUser = () => dispatch => {
  // Remove token from local storage
  localStorage.removeItem("Oauth");
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to empty object {} which will set isAuthenticated to false
  dispatch(setCurrentUser({}));
};
